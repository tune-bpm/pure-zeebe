package net.pervukhin.purezeebeclient.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Service
public class StartUpService {
    private Logger logger = LoggerFactory.getLogger(StartUpService.class);
    private static final long startTime = System.currentTimeMillis();

    @Autowired
    private ZeebeService zeebeService;

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        logger.info("На старт");
        load(100000);
    }

    public static long getStartTime() {
        return startTime;
    }

    private void load(int quantity) {
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < quantity; i++) {
            zeebeService.runProcess("tuneBPM", new HashMap<>());
        }
        long endTime = System.currentTimeMillis();
        logger.info("Время выполнения|"+ quantity +"|" + (endTime-startTime));
    }
}
