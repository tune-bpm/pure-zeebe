package net.pervukhin.purezeebeclient.service;

import io.camunda.zeebe.client.ZeebeClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class ZeebeService {
    @Autowired
    private ZeebeClient zeebeClient;

    public void runProcess(String processId, Map<String, Object> variables) {
        zeebeClient.newCreateInstanceCommand()
                .bpmnProcessId(processId)
                .latestVersion()
                .variables(variables)
                .send().join();
    }
}
