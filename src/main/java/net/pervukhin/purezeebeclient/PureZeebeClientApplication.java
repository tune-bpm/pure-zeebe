package net.pervukhin.purezeebeclient;

import io.camunda.zeebe.spring.client.EnableZeebeClient;
import io.camunda.zeebe.spring.client.annotation.ZeebeDeployment;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableZeebeClient
@ZeebeDeployment(resources = "classpath:tuneBPM.bpmn")
public class PureZeebeClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(PureZeebeClientApplication.class, args);
    }

}
