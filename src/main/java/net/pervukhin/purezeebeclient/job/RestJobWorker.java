package net.pervukhin.purezeebeclient.job;

import io.camunda.zeebe.client.api.response.ActivatedJob;
import io.camunda.zeebe.client.api.worker.JobClient;
import io.camunda.zeebe.spring.client.annotation.ZeebeWorker;
import net.pervukhin.purezeebeclient.service.StartUpService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;

@Service
public class RestJobWorker {
    private static final Logger logger = LoggerFactory.getLogger(RestJobWorker.class);
    private static final Random random = new Random();
    private static AtomicInteger count = new AtomicInteger(0);

    @Value("${rest.request.url}")
    private String restRequestUrl;

    @Autowired
    private RestTemplate restTemplate;

    @ZeebeWorker(type = "restJob")
    public void handleJob(JobClient jobClient, ActivatedJob activatedJob) {
        final String result = restTemplate.getForObject(restRequestUrl + "?v=" + random.nextLong(), String.class);
        logger.debug(result);

        final int currentCount = count.incrementAndGet();

        if (currentCount == 2000 || Math.floorMod(currentCount, 20000) == 0) {
            logger.warn("Время выполнения|"+ currentCount/2 +"|"
                    + (System.currentTimeMillis() - StartUpService.getStartTime()));
        }

        jobClient.newCompleteCommand(activatedJob.getKey())
                .variables(new HashMap<>())
                .send()
                .join();
    }
}
